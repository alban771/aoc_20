#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <list>
#include <set>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-7-input";
static const string mybagcolor = "shiny gold";

typedef int T;
typedef pair<string, T> bag;
typedef list<bag> contained;
typedef unordered_map<string, contained> policy;


ostream &operator<<( ostream & out, const bag & b) {
  return out << b.second << "-" << b.first;
}

ostream &operator<<( ostream & out, const policy::value_type & pe ){
  out << pe.first << " -> "; 

  if (pe.second.size() == 0)
    return out << "/" << endl;

  out << "{ ";
  auto e = pe.second.end();
  for_each( pe.second.begin(), --e, 
      [&](const bag & b) { out << b << ", ";});
  return out << pe.second.back() << " }" << endl;
}


contained & get_or_create( policy& p, const string& col ){
  if ( p.find( col ) == p.end() ) {
    contained c;
    p.emplace(col, c);
  }
  return p[col];
}


int main() {

  T k;
  string   i, j;
  fstream in(input);
  policy   p;
  
  while ( in >> i, in >> j ) 
  {
    string col =  i+" "+j;
    get_or_create( p, col );

    in >> i >> i; 
    while ( i.back() != '.' ) {
      in >> k >> i >> j;
      if ( k == 0 ) break;

      get_or_create(p, col).push_back( make_pair( i+" "+j, k ) );
      in >> i;
    }
  }


  /*
  for_each( p.begin(), p.end(), []( const policy::value_type & pe) { 
        cerr << pe; 
      });
      */

  contained r = p[mybagcolor];
  cerr << make_pair( mybagcolor, r ) << endl; 
  for_each( r.begin(), r.end(), 
      [&r,&p]( const bag & b ){ 
        cerr << *p.find(b.first);
        transform( p[b.first].begin(), p[b.first].end(), back_inserter(r), 
            [&b](const bag &bb) { return make_pair( bb.first, bb.second * b.second);});
        cerr << make_pair( mybagcolor, r ) << endl; 
      });


  cout << accumulate( r.begin(), r.end(), 0, [](const long long s, const bag b){ return s + b.second; }) << endl;
}
