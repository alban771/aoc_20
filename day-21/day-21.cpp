#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-21-input";


/**
 * Input parse
 */
struct receipe {
  set<string> ingredient, label;
};

istream & operator>>( istream& in, receipe &r ){
  string line, w;

  r.ingredient.clear();
  r.label.clear();
  if ( !getline(in, line) ) return in;
  stringstream ss(line);
  
  while( ss >> w && w != "(contains" ) 
    r.ingredient.insert(w);

  set<string>::iterator it;
  while( ss >> w, 
      it = r.label.insert(w).first, 
      w.back() != ')' )
    const_cast<string&>(*it).pop_back();

  const_cast<string&>(*it).pop_back();
//  cerr << decltype(*it) << endl;
//  cerr << "last label: " << *it << endl;

  return in;
}



/**
 *  Set intersection
 */

struct ingredients_map : public map<string, set<string>> 
{
  using base_t = map<string, set<string>>;
  void operator+=( receipe&& r ){
    for ( auto s : r.label )
    {
      set<string>& _old_set = base_t::operator[](s);
      if ( _old_set.empty() )
        _old_set = r.ingredient; 
      else
      {
        set<string> _new_set;
        auto _it = inserter( _new_set, _new_set.begin() );
        set_intersection(
            r.ingredient.begin(), r.ingredient.end(), 
            _old_set.begin(), _old_set.end(), _it
          );
        _old_set = _new_set;
      }
    }
  }
};



/**
 *  Look for result
 */

const bool operator<=( const set<string> lhs, const set<string> rhs )
{
  return lhs.size() < rhs.size();
};




int main() {

  ingredients_map  igt_map;
  {
    fstream   in(input);
    receipe r;
    while ( in >> r ) { 
      igt_map += std::move(r); 
    }
  }

  auto it = igt_map.begin();
  while ( 
    it = find_if( igt_map.begin(), igt_map.end(), 
        []( const auto& p )
        { 
          return p.second.size() == 1; 
        }), 
    it != igt_map.end() )
  {
    for_each( igt_map.begin(), igt_map.end(),
    [e=*it->second.begin()]( auto & p ){
        auto & [l, set] = p; 
        if (set.size() != 1)
          if (set.erase(e));
    });
    it->second.insert("__");
  }

  for_each( igt_map.begin(), igt_map.end(), 
      []( auto p ){ 
        auto& [l, igts] = p;
//        cerr << igts.size() << endl;
        cerr << "label: " << l << " ingredients #" << igts.size() << endl;
        cerr << ": [";
        copy( igts.begin(), igts.end(), ostream_iterator<string>(cerr, " ") );
        cerr << "]" << endl;
      });

  vector<string> igt_union;
  transform( igt_map.begin(), igt_map.end(), back_inserter(igt_union),
      []( const auto & p ){ return *++p.second.begin(); });

  int res_silver;
  {
    fstream   in(input);
    receipe r;
    while ( in >> r ) { 
      res_silver += count_if( r.ingredient.begin(), r.ingredient.end(), 
          [&igt_union]( const string& e )
          {
             return none_of(igt_union.begin(), igt_union.end(), 
                 [&](const string& s){ return s == e;});
          });
    }
  }
  cout << res_silver << endl;

  transform( igt_map.begin(), igt_map.end(), 
      ostream_iterator<string>(cout, ","), 
      []( auto &p ){
        return *++p.second.begin(); 
      });
  cout << endl;

}

