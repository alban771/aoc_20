#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-10-input";


typedef int T;

long long fib( int d ) {
  if ( d <= 1 )
    return 1;
  if ( d == 2 )
    return 2;

  return fib( d-1 ) + fib( d-2 ) + fib( d-3 ); 
}

int main() {

  T   i;
  fstream in(input);
  set<T>   c;
  vector<T> a; 
  
  while ( in >> i ) 
  {
    c.insert(i);
  }

  adjacent_difference(c.begin(), c.end(), back_inserter(a));

  cerr << "c: [ ";
  copy(c.begin(), --c.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  *--c.end() << " ]" << endl;

  cerr << "a: [ ";
  copy(a.begin(), --a.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  a.back() << " ]" << endl;

  //cout << count(a.begin(), a.end(), 1) * ( count(a.begin(), a.end(), 3) ) << endl;
  
  long long s = 1;
  vector<T>::iterator u = a.begin(), v;
  while ( u != a.end() ){
    if ( u != a.begin()) ++u;
    v = find( u, a.end(), 3);
    cerr << "d: " << distance(u,v) << endl;
    s *= fib( distance(u,v) );
    u = v;
  }


  cout << s << endl;
}
