#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-11-input";



template<size_t _l>
bool adjacent( const vector<string> &c, int i, int j ){
  int s = 0;
  for ( int ii = 0; ii < 9; ++ii ) {
    if ( ii == 4 ) continue;
    int k = 1;
    char _c;
    while ( _c = c[ i-k + k*(ii % 3) ][j-k + k*(ii/3)] , _c == '.' ) ++k;
    s += ( _c == '#' );
  }
  return s >= _l; 
}


int main() {

  bool stable = false;
  string   i;
  fstream in(input);
  vector<string>   c, d;

  
  while ( getline( in,  i) ) 
  {
    c.push_back(i);
  }
  d = c;

  while( !stable ) {

    copy(c.begin(), c.end(), ostream_iterator<string>(cerr, "\n") );
    stable = true;
    for (int i = 0; i < c.size(); ++i ) {
      for (int j=0; j<c[i].size(); ++j ) {
        if ( c[i][j] == 'L' && !adjacent<1>(c, i, j) ) {
          d[i][j] = '#';
          stable = false;
        }
        if ( c[i][j] == '#' && adjacent<5>(c, i, j) ) { 
          d[i][j] = 'L';
          stable = false;
        }
      }
    }

    c = d;
  }


  cout << accumulate(c.begin(), c.end(), 0, [](const int r, const string &s){ return r + count( s.begin(), s.end(), '#'); } ) << endl;
}
