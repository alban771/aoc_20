#include <iostream>
#include <fstream>
#include <set>
#include <iterator>
#include <algorithm>
#include <numeric>

using namespace std;

constexpr bool silver = false;

typedef int             val;
typedef set<val>        cont; 
typedef cont::iterator  iter; 


template< int rec, typename it = iter, typename T = val>
constexpr auto find_sum_parts =
[]( const it& b, const it& e, const T a ){
  return accumulate( b, e, 0, 
      [&] (const T s, const T v )
      { return s > 0 ? s : find_sum_parts<rec-1>(b, e, a-v) * v; });   
};


// Rq : partial specialization not authorized for functions
//      is a pb for type inference 
template< typename it, typename T>
constexpr auto find_sum_parts<2,it,T> = 
[]( const it& b, const it& e, const T a ){
  it _b=b, _e=e;
  --_e;
  while( *_b + *_e != a && *_b <= *_e ){
    if ( *_b < a - *_e )
      ++_b;
    else 
      --_e;
  }
  return *_b + *_e == a ? *_e * *_b : 0;
};

    

int main() {

  fstream in("day-1.input");
  val i = 0;
  cont s;
  while ( in >> i ) s.insert(i);

  if constexpr ( silver ) 
  {
    cout << find_sum_parts<2>( s.begin(), s.end(), 2020 );
  } 
  else 
  {
    cout << find_sum_parts<3>( s.begin(), s.end(), 2020 );
  }
}
