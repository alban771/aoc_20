#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <complex>
#include <algorithm>
#include <iterator>
#include <numeric>
#include "2D_mapping.h"


using namespace std;
using map_2d::index_A;
using map_2d::yz;


static const string input = "day-17-input";
//static const string input = "day-17-eg1";


constexpr int N_CYCLE = 6;
constexpr int DIM_X_0 = 8;
constexpr int DIM_Y_0 = 8;


typedef index_A<DIM_Y_0>          index;
typedef vector<bool>              row;
typedef vector<row>               space;
typedef tuple<int,int,int,int>    xyzw;


static index IND;



static bool get( const xyzw& pos, const space& s ) {
  int x,y,z,w;
  tie( x,y,z,w ) = pos; 

  size_t i,j;
  {
    const size_t st = IND.stage;
    i = IND.unmap( { y, z } );
    j = IND.unmap( { x, w } );
    IND.stage = st;
  }
  return s.size() <= i       ? false :
         s.at(i).size() <= j ? false :
         s.at(i).at(j);
}


static vector<xyzw> neighbours( const xyzw& pos ) {
  vector<xyzw> ngh(80);
  generate(ngh.begin(), ngh.end(), 
      [&pos,i=-1] () mutable -> xyzw {
        int x,y,z,w;
        tie( x,y,z,w ) = pos; 
        if ( ++i == 40 ) ++i;
        return {  x - 1 + i % 3, 
                  y - 1 + (i/3) % 3, 
                  z - 1 + (i/9) % 3,
                  w - 1 + (i/27)      };
      });
  return ngh;
}


static bool next( const xyzw& pos, const space& s ){
  vector<xyzw> ngh = neighbours(pos);
  int arity = count_if( ngh.begin(), ngh.end(), 
      [&s](const xyzw& n ){ return get( n, s ); });
  return arity == 3 || ( arity == 2 && get(pos, s));
}



static void set( const xyzw& pos, space& s, bool val ) {
  int x,y,z,w;
  tie( x,y,z,w ) = pos; 
  size_t i, j;
  {
    const int st = IND.stage;
    i = IND.unmap( { y, z } );
    j = IND.unmap( { x, w } );
    IND.stage = st;
  }
  size_t dim_xw = IND.size();
  if ( i >= s.size() ) 
  { 
    const space::value_type def( dim_xw ); 
    s.resize( IND.size(), def);
  }
  if ( s[i].size() <= j ) 
  {
    s[i].resize( dim_xw, 0 );
  }
  s[i][j] = val;
}


istream & operator>>(istream & in, space & s ) {
  char c;
  for ( int y = DIM_Y_0; --y >= 0; ) 
  { 
    for ( int x = -1; ++x < DIM_X_0; ) 
    {
      in >> c; 
      set( {x,y,0,0}, s, c == '#' );
    }
  }
  return in;
}


namespace std {
  ostream & operator<<(ostream & out, const deque<bool> & r ) {

    for_each( r.begin(), r.end(), 
        [&](const bool b){
            out << ( b ? " #" : " ." );
        });
    return out << endl;
  }
}

ostream & operator<<(ostream & out, const space & s ) {

  IND.map( s.size() - 1 );
  const size_t st = IND.stage;
  size_t dim_xy = IND.dim_y(0);
  size_t dim_zw = IND.dim_z(0);


  vector<int> Z(dim_zw);
  vector<int> Y(dim_xy);
  iota( Y.begin(), Y.end(), IND.off_y(0) );
  iota( Z.begin(), Z.end(), IND.off_z(0) );

  for_each ( Z.begin(), Z.end(), 
      [&]( const int z ){
        out << "z:" << z << ", ";
        for_each( Z.begin(), Z.end(), 
          [&]( const int w ){
            out << "w:" << w << endl;
            transform( Y.rbegin(), Y.rend(), ostream_iterator<deque<bool>>(out), 
                [&](const int y){

                  row row = s[ IND.unmap( {y, z} ) ];
                  size_t p = IND.unmap({0, w}); // set IND.stage according to w
                  int off_x = IND.off_y(w);     
                  int dim_x = IND.dim_y(w);
                  p += off_x;
                  deque<bool> _r( dim_x );

                  auto x1 = next( row.cbegin(), p ); // copy linear part (specific to index_A)
                  auto x2 = next( x1, dim_x );
                  copy( x1, x2, _r.begin() ); 

                  int h = (dim_xy - _r.size())/2; // add tail and head
                  for ( int i = 0; ++i <= h; )
                  {
                    _r.push_front( row[ IND.unmap( { off_x - i, w } ) ] ); 
                    _r.push_back( row[ IND.unmap( { off_x + dim_x - 1 + i, w } ) ] );
                  }
                    
                  return _r; 
                });
            out << endl;
          });
      });

  IND.stage = st;
  return out;
}


int main() {

  fstream in(input);
  space  s;
  in >> s; 
  cerr << "stage:0" << endl << s << endl;


  while ( ++IND.stage <= N_CYCLE ) 
  {
    space s1; 
    const size_t S = IND.size();
    index mapper0 = IND, mapper1 = IND;
    for ( size_t k = 0; k < S; ++k )
    {
      yz a = mapper0.map(k);
      for ( size_t l = 0; l < S; ++l ) 
      {
        yz b = mapper1.map(l);
        xyzw pos = { b.real(), a.real(), a.imag(), b.imag() };
        set( pos, s1, next( pos, s ) );
      }
    }
    swap(s, s1);
    cerr << "stage:" << IND.stage << endl << s << endl;
  }

  
  cout << accumulate(s.begin(), s.end(), 0, 
      [](const long t, const row &r){ 
        return t + count( r.begin(), r.end(), 1); 
      }) << endl;
}
