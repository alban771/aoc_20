#include <iostream>
#include <deque>
#include <utility>
#include <algorithm>
#include <complex>


namespace map_2d {

using namespace std;



typedef complex<int> yz;

struct map_2d {

  int stage = 0;

  constexpr map_2d(){}

  constexpr map_2d( const map_2d & o ) : stage(o.stage) {}

  virtual map_2d &operator++() { ++stage; return *this; }
  
  virtual map_2d &operator--() { --stage; return *this; }

  virtual const int off_y( int ) const = 0;

  virtual const size_t dim_y( int ) const = 0;

  virtual const size_t dim_z( int ) const = 0;
  
  const int off_z( int y ) { return - (int) dim_z(y) / 2; }

  virtual const size_t size() const = 0;

  virtual const size_t unmap( const yz & ) = 0;

  virtual const yz map( const size_t ) = 0;
};

typedef deque<size_t> row;
ostream & operator<<( ostream & out, const row & r )
{
      for_each( r.begin(), r.end(), 
          [&out](size_t x){ out << x << ( x<10?"   ":x<100?"  " :" "); });
      return out << endl;
}

ostream & operator<<( ostream & out, map_2d & map ) 
{
  deque< row > d; 
  const int S = map.stage;
  map.stage=-1; 

  for ( size_t m=0, _m=0; m=(++map).size(), map.stage < S; )
  {
    d.emplace_front();
    if ( map.stage )
      d.emplace_back();
    
    for ( ; _m < m; ++_m ) 
    {
      yz p = map.map(_m);
      int i = map.stage + p.imag();
      

      if ( p.real() - (int)d[i].size() < map.off_y( p.imag() ) )
        d[i].push_front(_m);

      else 
        d[i].push_back(_m);
    }
  }

  for_each( d.rbegin(), d.rend(), 
      [&out]( const row & q ){ out << "\t\t" << q;  });
  return out;
}

ostream & operator<<( ostream & out, const yz & yz ) {
  return out << "y:" << yz.real() << " z:" << yz.imag();
}

const bool assert_unmap( map_2d & m )
{
  size_t i = -1;
  const size_t s = m.size();
  while ( ++i < s && m.unmap( m.map(i) ) == i );  
  return i == s;
}



/*
 
 z
 ^
 |
 0 --> y
  
 version A:

     36 37 38 39 40 41 42 43 44
     26 14 15 16 17 18 19 20 25
     22 6  0  1  2  3  4  5  21
     24 7  8  9  10 11 12 13 23
     27 28 29 30 31 32 33 34 35
 
  version B:

     36 37 38 39 40 41 42 43 44
     35 14 15 16 17 18 19 20 21
     34 13  0  1  2  3  4  5 22
     33 12 11 10  9  8  7  6 23
     32 31 30 29 28 27 26 25 24


 */


template<int DIM_Y_0 = 5>
struct index_A : map_2d {


  const size_t size() const  
  {
    return stage < 0 ? 0 : dim_y(0) * dim_z(0);
  }

  const int off_y( int z ) const 
  {
    return -stage;
  }

  const size_t dim_y( int z ) const 
  {
    return DIM_Y_0 + 2 * stage;
  }

  const size_t dim_z( int y ) const
  {
    return 1 + 2 * stage;
  }


  const yz map( const size_t pos ) 
  {
    // Todo: solve this by analyse
    size_t m=0;
    for ( size_t _m; _m = size(), pos < _m ; --stage, m=_m );
    if ( !m )
      while( m=size(), pos >= m ) ++stage;
    else 
      ++stage;

    int s = stage;
//    cerr << " s:" << s;

    int y, z;
    size_t _dim_y = dim_y(0);

    if ( m - pos <= _dim_y )
    {
      z = (dim_z(0) - 1) / 2;
      y = pos - m + _dim_y;
    }

    else if ( m - pos <= 2 * _dim_y ) 
    {
      z = (1 - dim_z(0)) / 2;
      y = pos - m + 2 * _dim_y;
    }

    else 
    {
      index_A inf = *this;
      size_t h = pos - (--inf).size();
      y = h % 2 ? 0 : _dim_y-1; 
      h /= 2; 
      z = h < s ? -h : h - s + 1;
    }
    
    return { y + off_y(0), z };
  }


  const size_t unmap( const yz & yz ) {
    
    stage = max ( 
        abs( yz.imag() ), 
        yz.real() > 0 ? yz.real() - DIM_Y_0 + 1 : - yz.real()
    );
    size_t m = size();
    int s = stage;

//    cerr << "s:" << s << " ";

    if ( yz.imag() == s )
      return m - dim_y(0) - off_y(0) + yz.real();
    
    if ( yz.imag() == -s )
      return m - 2*dim_y(0) - off_y(0) + yz.real();

    index_A inf = *this;
    return (--inf).size()
      + 2 * ( yz.imag() <= 0 ? -yz.imag() : (s-1) + yz.imag() ) 
      + ( yz.real() < 0 );
  }

};


/*
 * 4 d^2 + 2 (DIM_Y_0 + 1) d + y_len = s
 *
 * dmin = -b/2a
 * /\   = b^2 - 4ac 
 *
 * 2 d + (DIM_Y_0 + 1)/2 = 0
 *
 */




/*
const size_t index_B( const yz & yz ) {

  size_t dim_z = dim_z_(yz);  
  size_t dim_y = y_len + 2 * dim_z;
  size_t s = size_( dim_z );

  if ( yz.imag() == dim_z )
    return s - dim_y + dim_z + yz.real();
  
  if ( yz.imag() == -dim_z )
    return s - 2*dim_y + dim_z + yz.real();

  return s - 2*dim_y - 4*( dim_z - abs(yz.imag()) ) + 2 * ( yz.imag() >= 0 ) + ( yz.real() > 0 );
}
*/


}



