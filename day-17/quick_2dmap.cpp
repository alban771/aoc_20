#include <numeric>
#include "2D_mapping.h"

using namespace std; 
using namespace map_2d;

int main() {

  index_A ia;

  ia.stage = 6;

  vector<size_t> v( 2 * ia.dim_z(0)  );

  iota(v.begin(), v.end(), (--ia).size() );

  cerr << "len stage " << ++ia.stage << ": " << ia.size() << endl;

  for_each(v.begin(), v.end(), 
      [&ia](const size_t s){
        yz m = ia.map(s); 
        cout << s << " -> " << m << " -> " << ia.unmap(m) << endl;
      });


  cout << endl << endl;

  cout << ia << endl << endl;


  cout << "test umap sequence up to " << ia.size() << ": " << boolalpha << assert_unmap(ia) << endl << endl;

}


