#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string Input = "day-5-input";


typedef int T;


auto seat = []( const string s )
{
  return accumulate( s.begin(), s.end(), 0, 
      [](const T p, const char n){ return ( p<<1 ) + ( n == 'B' || n == 'R' );} );
};


int main() {
  T   i;
  string s;
  fstream in(Input);
  set<T>   c;
  
  while ( in >> s ) 
  {
    T k = seat(s);
    c.insert( k );
    cerr << "seat: " << k << " s: " << s << endl;
  }

  for( auto b = c.begin(); b != c.end(); ++b  )
    cerr << "seat: " << *b << endl;

  vector<T> d(c.size());
  adjacent_difference( c.begin(), c.end(), d.begin() );

  //cerr << count( d.begin(), d.end(), 1 ) << endl;

  cout << distance( d.begin(), find(d.begin(), d.end(), 2) ) + d.front() << endl;
}
