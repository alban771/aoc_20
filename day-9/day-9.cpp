#include <iostream>
#include <fstream>
#include <set>
#include <list>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-9-input";

constexpr int preambule = 25;


typedef int T;

const auto sum_of_two( set<T> c, T s ) { 
  auto b = c.begin();
  auto e = c.rbegin();

  while ( *b + *e != s && *b < *e ){
    if ( *b + *e < s )
      ++b;
    else
      ++e;
  }
  return *b + *e == s;
}

int main() {

  T   i;
  set<T>   c;
  list<T>   l, sp;
  int j = 0;
  fstream in(input);
  
  for (; j < preambule; ++j) { 
    in >> i; 
    c.insert( i ); 
    l.push_back(i);
  }
  

  cerr << "[ ";
  copy(l.begin(), --l.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  *--l.end() << " ]" << endl ;
  while ( ++j, in >> i ) 
  {
    if ( !sum_of_two(c, i) )
      break;

    c.erase( l.front() ); 
    c.insert( i );
    l.pop_front();
    l.push_back(i);
  }

  cerr << i << endl;

  T u;
  l.clear();
  in.close();
  in.open(input);




  sp.push_back( 0 );
  list<T>::iterator w = sp.begin();

  while ( in >> u ) {

    l.push_back( u );

    transform( w, sp.end(), sp.begin(), [&u]( const T v ){ return u+v; });
    w = find_if( w, sp.end(), [&i](const T v){ return v <= i; });

    sp.erase( sp.begin(), w );
    l.erase( l.begin(), prev(l.end(), sp.size())); 
    sp.push_back( 0 );


    /*
    if ( l.size() == 0 ) 
      cerr << "l: []" << endl;
    else {
      cerr << "l: [ ";
      copy( l.begin(), --l.end(), ostream_iterator<T>(cerr, ", ") );
      cerr <<  l.back() << " ]" << endl ;
    }


    if ( sp.size() == 0 ) 
      cerr << "sp: []" << endl;
    else {
      cerr << " sp: [ ";
      copy(sp.begin(), --sp.end(), ostream_iterator<T>(cerr, ", ") );
      cerr <<  *--sp.end() << " ]" << endl ;
    }
    */


    if ( *w == i ) 
      break;

  }


  cout << *max_element( l.begin(), l.end() ) + *min_element( l.begin(), l.end()) << endl;




}

