#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <map>
#include <array>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
//#include <execution>


using namespace std;


static const string input = "day-19-input";
//static const string input = "day-19-eg2";

/*
 *  Rule parser
 *  
 */
typedef array<size_t,2>    seq;
typedef pair<seq,seq>     disjn;

template<typename r_type>
struct R {
  typedef r_type value_type;
  r_type r;
  R( const r_type & _r ):r(_r){}
  ~R(){}
  const size_t match( const string & w, const size_t pos ) const;
  void operator=( const R<r_type> &oth ){ r = oth.r; }
};


/*
template<>
R<seq>::R():r(2){}

template<>
R<disjn>::R():r(make_pair<seq,seq>(2,2)){}
*/

template<>
const size_t R<char>::match( const string &w, const size_t pos ) const 
{
  return ( w[pos] == r ) ? pos+1 : string::npos;
}

static const size_t seq_match( const seq & r, const string &w, const size_t pos );

template<>
const size_t R<seq>::match( const string &w, const size_t pos ) const 
{
  return seq_match( r, w, pos);
}


template<>
const size_t R<disjn>::match( const string &w, const size_t pos ) const 
{
  const size_t _pos = seq_match( r.first, w, pos );
  return _pos == string::npos ? seq_match( r.second, w, pos ) : _pos;
}



//template<>
ostream & operator<<( ostream& out, const R<char>& r ){
  return out << "\"" << r.r << '"'; 
}

ostream & operator<<( ostream& out, const seq& t ){
  copy( t.begin(), t.end(), ostream_iterator<size_t>(out, " ") );
  return out;
}

//template<>
ostream & operator<<( ostream& out, const R<seq>& r ){
  return out << r.r;
}

//template<>
ostream & operator<<( ostream& out, const R<disjn>& r ){
  return out << r.r.first << " | " << r.r.second;
}


struct r_u {
  enum { CHAR, SEQ, DISJN } tag;
  union {
    R<char> c; 
    R<seq>  s; 
    R<disjn>  d;
  };
  r_u():tag(CHAR){}
  r_u( const r_u &oth ):tag(oth.tag){
    switch (tag) {
      case CHAR:
        c = oth.c;
      case SEQ:
        s = oth.s;
      case DISJN:
        d = oth.d;
    }
  }
  ~r_u(){}
  const size_t match( const string & w, const size_t pos ) const {
    switch( tag ) {
      case CHAR:
        return c.match(w, pos);
      case SEQ:
        return s.match(w, pos);
      case DISJN:
        return d.match(w, pos);
    }
    return string::npos;
  }
};

ostream & operator<<( ostream& out, const r_u& r ){
  switch (r.tag){
    case r_u::CHAR:
      return out << r.c;
    case r_u::SEQ:
      return out << r.s;
    case r_u::DISJN:
      return out << r.d;
  }
  return out;
}






/*
template<>
R<seq>::~R(){ r.clear(); }
template<>
R<disjn>::~R(){ r.first.clear(); r.second.clear(); }
*/

typedef map<size_t, r_u>  r_map;

istream & operator>>( istream & in, r_u & ru) {

  if ( in.peek() == '"') {
    string s;
    in >> s;
    ru.c = R<char>(s[1]);
    ru.tag = r_u::CHAR;
    return in;
  }

  seq t {0, string::npos}; 
  for ( int i=0; in >> t[i], in.peek() != '\n'; ++i) {
    in.ignore();
    if ( in.peek() == '|' ) break;
  }

  if ( in.peek() == '\n' ) {
    ru.s = R<seq>(t);
    ru.tag = r_u::SEQ;
    return in;
  }

  in.ignore(2,'|');
  seq q {0, string::npos};
  for ( int i=0; in >> q[i], in.peek() != '\n'; ++i);
  ru.d = R<disjn>(make_pair(t,q));
  ru.tag = r_u::DISJN;
  return in;
}




istream & operator>>( istream& in, r_map & r ){
  size_t k;
  r_u ru;
  in >> k;
  in.ignore(2);
  in >> ru;
  cerr << ru << endl;
  r.emplace( k, ru );
  in.ignore(1,'\n');
  return in;
}

static r_map R_MAP; 

static const size_t seq_match( const seq & r, const string &w, const size_t pos ) {
    return accumulate( r.begin(), r.end(), pos, 
      [&w]( const size_t _pos, const size_t k ){
        return _pos == string::npos || k == string::npos ? _pos : R_MAP[k].match(w, _pos);
      });
};

ostream & operator<<( ostream& out, const r_map& rm ){
  for_each( rm.begin(), rm.end(), 
      [&out]( const r_map::value_type & p ){
        out << p.first << ": " << p.second << endl;  
      });
  return out;
}






int main() {

  string  w;
  fstream in(input);
  
  while ( in.peek() != '\n' ) in >> R_MAP; 
  in.ignore();

  cerr << R_MAP;

  size_t s = 0, g = 0;
  R<disjn> &pre = R_MAP[42].d, &suf = R_MAP[31].d; 

  while ( in >> w )
  {
    bool slv, gld;
    slv = R_MAP[0].match(w, 0) == w.size();

    if ( slv ){

      size_t asrt = pre.match(w, 0);
      asrt = pre.match(w, asrt);
      asrt = suf.match(w, asrt);
      assert( asrt == w.size() );
    }

    vector<size_t> cdt; 
    size_t pos = pre.match(w, 0);
    while ( pos = pre.match(w, pos), pos != string::npos ) cdt.push_back(pos);

    transform( cdt.begin(), cdt.end(), cdt.begin(), 
        [&suf,&w]( size_t _pos ) { return suf.match(w, _pos);  });

    gld = any_of( cdt.begin(), cdt.end(), 
        [&suf,&w,i=0]( size_t _pos ) mutable {
          for ( int j=++i; _pos != string::npos && _pos != w.size() && --j > 0; )
              _pos = suf.match(w, _pos);  
          return _pos == w.size();
        });

    if ( slv && gld )
      cerr << "slv & gld: " << w << endl; 
    else if ( slv )
      cerr << "slv: " << w << endl; 
    else if ( gld )
      cerr << "gld: " << w << endl; 
    else 
      cerr << "no match: " << w << endl; 

    s += slv;
    g += gld;
  }

  cout << "slv: " << s << " gld: " << g << endl;
}
