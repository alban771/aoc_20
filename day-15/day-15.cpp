#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-15-input";
//static const string input = "day-15-eg1";
//static const string input = "day-15-eg2";


typedef unsigned long long T;
/*
istream & operator>>( istream& in,  T ){
}

ostream & operator>>( ostream& in,  T ){
}
*/


typedef map<T,T> container;


int main() {

  T   i=0, k, l;
  fstream in(input);
  container  c;
  

  // missing virgula after last value
  in >> l;
  while ( in.ignore(), in >> k ) { 
    c[l]=++i;
    l = k;
  }


  while ( ++i < 30'000'000ll ) 
  //while ( ++i < 2020 ) 
  {
    auto f = c.find( k );
    l = ( f == c.end() ? 0 : i - f->second ); 
    c[k] = i; 
    k = l;

    if ( i == 8 || i == 10 || i == 2019 ) {
      cerr << "[ " ;
      for_each( c.begin(), c.end(), []( const auto p ){cerr << p.first << ":" << p.second <<" "; } );
      cerr << "] k: " << k << endl;
    
    }

  }

  cout << k << endl;
}
