#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <complex>


using namespace std;


static const string input = "day-12-input";

typedef complex<double> T;

static const T N = 1i, S=-1i, E=1.0+0i, W=0i-1.0; 
static T wp = 10.0*E + N;


//typedef int T;
//typedef set<T> container; 


typedef vector<T> container; 


T getCood( string l ){

  double len = stoi(l.substr(1, l.size()-1)); 
  if ( l[0] == 'N' ) 
    wp += len * N;
  if ( l[0] == 'E' )
    wp += len * E;
  if ( l[0] == 'S' )
    wp += len * S;
  if ( l[0] == 'W' )
    wp += len * W;
  if ( l[0] == 'L' ) 
    wp *= pow( 1i, ( len / 90.0 ));
  if ( l[0] == 'R' )  
    wp *= pow( -1i , ( len / 90.0 ));
  if ( l[0] == 'F' ) 
    return wp * len;
  return 0;
}

int main() {

  string  i;
  container   c;
  fstream in(input);
  
  //while ( getline( in, i ) ) 
  while ( in >> i ) 
  {
    //c.insert(i);
    c.push_back( getCood( i ));
  }


  cerr << "[ " ; 
  copy(c.begin(), --c.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  c.back() << " ]" << endl;



  T pos = accumulate( c.begin(), c.end(), (T) 0i );
  cerr << pos;
  cout <<  pos.real() + pos.imag() * ( pos.imag() * pos.real() > 0 ? 1 : -1) << endl;
}
