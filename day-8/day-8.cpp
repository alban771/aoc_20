#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;



static const string input = "day-8-input";

typedef int T;




int main() {

  string l;
  fstream in(input);
  vector<string>   c;
  
  while ( getline( in, l) ) 
  {
    c.push_back(l);
  }

 
  T i=0,acc=0;
  vector<int> pos;

  const auto compileur = [&acc]( const string l ) {
    T arg = stoi( l.substr(3,l.size() - 3) );
    if ( l[0] == 'a' ) { 
      acc += arg;
      return 1;
    }
    if ( l[0] == 'n' ) { 
      return 1;
    }
    if ( l[0] == 'j' ) { 
      return arg;
    }
    return 0;
  };
  
  for( int j = 0; j < c.size(); i=0, acc=0, pos.clear(), ++j ) {

      if ( c[j][0] == 'a' ) 
        continue;

      c[j][0] = ( c[j][0] == 'n' ? 'j' : 'n');

      //cerr << "cmd: " << c[i] << " i: " << i << " acc: " << acc << endl;
      while ( find( pos.begin(), pos.end(), i) == pos.end() && i < c.size() ){
        pos.push_back(i);
        i += compileur(c[i]);
        //cerr << "cmd: " << c[i] << " i: " << i << " acc: " << acc << endl;
      }

      cerr << "change at: " << j << " l: " << c[j] << " acc: " << acc << " final pos: " << i << endl;
      c[j][0] = ( c[j][0] == 'n' ? 'j' : 'n');

      if ( i >= c.size() ) {
        cout << acc << endl;
        break;
      }
  }


}
