#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string input = "day-0-input";
//static const string input = "day-0-eg1";
//static const string input = "day-0-eg2";


typedef int T;
/*
istream & operator>>( istream& in,  T ){
}

ostream & operator>>( ostream& in,  T ){
}
*/


typedef vector<T> container;


int main() {

  T   i;
  fstream in(input);
  container  c;
  
  while ( in >> i ) 
  {
    c.push_back(i);
  }


  cerr << "[ " ;
  copy(c.begin(), --c.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  c.back() << " ]" << endl;


  cout << accumulate(c.begin(), c.end(), 1, multiplies<T>() ) << endl;
}
