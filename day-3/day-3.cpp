#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>


using namespace std;




int main () {

  string s;
  int i=-1;
  fstream in("day-3-full.input");
  
  vector<float> direction{1,3,5,7,0.5};
  vector<size_t> r(direction.size(), 0);

  while ( ++i, in >> s ) { 
    transform( direction.begin(), direction.end(), r.begin(), r.begin(),
        [&s,&i](const float f, const int j){
          if ( (f * i) != floor(f * i) )
            return j;
          int k = ((int) floor(f * i)) % s.size();
          return j + ( s[ k ] == '#' );
        });
    if ( ! ( i % 5) ) {
      copy( r.begin(), r.end(), ostream_iterator<size_t>(cerr, " ") );
      cerr << endl;
    }
  }

  copy( r.begin(), r.end(), ostream_iterator<size_t>(cerr, " ") );
  cerr << endl;
  cout << accumulate( r.begin(), r.end(), 1, 
      [](const unsigned long long u, const int v){ return u*v; }) << endl;
}
