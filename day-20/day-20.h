#include <list>
#include <string>
#include <iterator>

using namespace std;


constexpr char ori_list[] = {'N', 'E', 'S', 'W'};

struct picture;

struct frame 
{
  typedef list<bool> bande_t;
  typedef list<bande_t>::iterator bande_it;
  typedef list<bande_t>::const_iterator bande_cit;

  frame() {};
  frame( const frame && o, int d = 0, bool r = false ): 
    label(o.label), bandes( o.bandes.size()),
    ori_incr(o.ori_incr+d), picture(move(o.picture))
  {
    generate( bandes.begin(), bandes.end(), 
        [&o,i=next( o.bandes.begin(), d )]() mutable {
          if ( i == o.bandes.end() ) 
            i = o.bandes.begin();
          return move(*i++); 
        });
    if ( r )
      for_each( bandes.begin(), bandes.end(),  
          []( bande_t &b ){ b.reverse(); });
  }

  template< char orientation = 'E' >
  frame operator> ( const frame & o ) ; 
  frame operator< ( const frame & o ) ; 
  frame operator^ ( const frame & o ) ; 
  bool operator== ( const frame & o ) const { return o.label == label; }
  bool operator!= ( const frame & o ) const { return o.label != label; }
  operator bool() const { return label != 0; }

  frame operator<< ( int u ) const 
  {
    frame f = *this;
    for ( ; u-- > 0 ; )
    {
      f.bandes.push_back( bandes.front() );
      f.bandes.pop_front();
      ++ori_incr;
    }
    return f;
  };


  int label = 0;
  list<bande_t> bandes;
  int ori_incr = 0;

  picture picture;
//  bool reverse;
};


constexpr auto ctob = [](const char c) { return c == '#'; };
constexpr auto btoc = [](const bool b) { return b ? '#' : '.'; };

bool operator>>( istream & in, frame &f ) {
    string l;
    getline( in, l );
    if ( !l.size() )
      return false;
    f.label = stoi( l.substr(5, 4) );

    frame::bande_t n, e, w, s;
    list<framte::bande_t> pic;
    getline( in, l);
    transform( l.begin(), l.end(), back_inserter(n), ctob );

    for ( ; e.push_back( ctob(l[9]) ),
            w.push_front( ctob(l[0]) ),
            pic.emplace_back(),
            transform( ++l.begin(), --l.end(), back_inserter(pic.back()), ctob ),
            in.peek() != '\n'; ) getline( in, l );

    transform( l.rbegin(), l.rend(), back_inserter(s), ctob );
    f.bandes = { n, e, s, w };
    f.picture = move(pic);
    in.ignore();
    return true;
}


ostream & operator<<( ostream & out, const frame &f ) {
  out << "label: " << f.label << endl;
  for_each( f.bandes.cbegin(), f.bandes.cend(), 
      [&,o = ori_list.begin()]( const frame::bande_t & b ) mutable {
        out << *o++  << ": ";
        transform ( b.begin(), b.end(), ostream_iterator<char>(out," "), 
            btoc );
        out << endl; 
      });
  return out;
}



/*

namespace std {
  template
  bool list<bool>::operator==( const list<bool> & ) {
    return 
  }
}

*/



template< char orientation >
frame frame::operator> ( const frame & o ) {
  bande_it i = bandes.begin(); 
  bande_cit n;
  find_if( ori_list.begin(), ori_list.end(), 
      [&i] ( const char c ) {
        ++i;
        return c == orientation;
      });
  --i;

  long int off_ori = 2 + 4 - distance( bandes.begin(), i );

  n = find( o.bandes.begin(), o.bandes.end(), *i );
  if ( n != o.bandes.end() ) 
    return { o, ( distance( o.bandes.begin(), n) + off_ori ) % 4 };

  i->reverse();
  n = find( o.bandes.begin(), o.bandes.end(), *i );
  i->reverse();
  if ( n != o.bandes.end() ) 
    return { o, ( distance( o.bandes.begin(), n) + off_ori ) % 4, true };
  else
    return {};
}

frame frame::operator< ( const frame & o ) {
  return this->operator><'W'>(o);
} 

frame frame::operator^ ( const frame & o ) {
  return this->operator><'N'>(o);
} 



/**
 *
 *
 *
 */

struct picture {

  using bande_t   = frame::bande_t;
  using bande_it  = frame::bande_it;
  using bande_cit = frame::bande_cit;
  using size_type = bande_t::size_type;

  picture() : 
    _l{{}}
  {}
  picture( list<bande_t> && l ) : 
    _l( forward(l) )
  {}

  size_type dim_v() const { return _l.size(); }
  size_type dim_h() const { return dim_v() ? _l.front().size() : 0; }
  
  picture & flip_v() noexcept
  { 
    _l.reverse(); return *this; 
  } 
  picture & flip_h() noexcept
  {
    for_each( _l.begin(), _l.end(), 
        []( bande_t & b ) { b.reverse(); });
    return *this;
  }

  picture & rotate_pos() noexcept
  {
    if ( !_l.size() ) return *this;

    list<bande_cit> _l_it( dim_v() );
    transform( _l.begin(), _l.end(), _l_it.begin(), 
        [](const bande_t & b) { return b.rbegin(); });

    list<bande_t> && _c( dim_h() );
    generate(_c.begin(), _c.end(), 
        [&_l_it]() mutable { 
          bande_t _nc;
          transform( _l_it.begin(), _l_it.end(), back_inserter(_nc), 
              []( bande_cit i ){ return *i++; });
          return _nc; 
        });

    _l = move(_c);
    return *this;
  }
  picture & rotate_negativ() noexcept 
  {
    flip_v();
    rotate_positiv();
    flip_v();
    return *this;
  }

  // TODO: optimize this!!
  picture & push_east( picture && p ) 
  {
    assert( p.dim_v() == dim_v() );
    for_each( p._l.begin(), p._l.end(), 
        [i = _l.begin()]( const bande_t &b ) mutable {
          copy( b.begin(), b.end(), back_inserter(*i++) );
        });
    return *this;
  }
  picture & push_west( picture && p ) {
    flip_h();
    p.flip_h();
    push_east( move(p) );
    flip_h();
    return *this;
  }
  friend picture & operator<<( picture & lhs, picture && rhs ) 
  { 
    return lhs.push_east( rhs );
  }
  friend picture & operator>>( picture && lhs, picture & rhs ) 
  { 
    return rhs.push_west( lhs );
  }

  // TODO: optimize me
  picture & push_north( picture && p ) 
  {
    assert( p.dim_h() == dim_h() );
    copy( p._l.rbegin(), p._l.rend(), front_inserter(_l) );
    return *this;
  }
  picture & push_south( picture && p ) 
  {
    flip_v();
    p.flip_v();
    push_north( move(p) );
    flip_v();
    return *this;
  }

private:
  list<bande_t> _l;

  friend picture_view;
};



/*
  friend picture_iterator;

  bande_cit find( const picture && p ) {

    assert( dim_h() >= p.dim_h() && dim_v() >= p.dim_v() );
    bande_it s;

    _iterator<dim_v() - p.dim_v(), dim_h() - p.dim_h()> 
      _it( _l.begin(), _l.begin().begin() );

    find_if( _it, _it.end(), 
          [&,i=_l.begin()]( const bande_t & b ){ 
          return any_of( i->begin(), previous(i++->end(), p.dim_h()), 
              [&,j=-1]( const auto v ) mutable { 
              ++j;
              return all_of( p._l.begin(), p._l.end(), 
                    [&,k=i]( const bande_t & _b ){
                    return all_of(_b.begin(), _b.end(), 
                      [&,l=next(k++->begin(), j)]( const bool v ){ 
                        return *(l++) || !v; 
                      });
                  });
            });
        });
  } 

  */



struct picture_view {
  using type_it_v = list<bande_it>::iterator;
  using type_it_h = picture::bande_it;


  picture_view( 
      const type_it_v bv, const type_it_v ev, 
      const type_it_h bh, const type_it_h eh ):
    beg_v_(bv), end_v_(ev),
    beg_h_(bh), end_h_(eh)
  {}
  picture_view( const picture & pic ):
    beg_v_( pic._l.begin() ), 
    end_v_( pic._l.end() ), 
    beg_h_( pic._l.front().begin() ), 
    end_h_( pic._l.front().end() ) 
  {}


  bool operator=>( const picture_view & o ) const 
  {
    size_t off_h = beg_h_ - beg_v_->begin();
    size_t dim_h = end_h_ - beg_h_;
    return mismatch( beg_v_, end_v_, o.beg_v_, 
        [off_h, dim_h]( const bande_t & b, const bande_t & ob ){
      return mismatch(  next(b.begin(), off_h), 
                        next(b.begin(), off_h + dim_h),
                        next(ob.begin(), off_h ),
          []( const bool b1, const bool b2 ){ return !b1 || (b1 & b2); }
          ) == next(b.begin(), off_h + dim_h);
    }) == end_v_;
  }


private:
  type_it_v beg_v_, end_v_;
  type_it_h beg_h_, end_h_;
};


struct picture_scanner {

  using value_type = picture_view;
  using difference_type = std::ptrdiff_t;
  using pointer = picture_view const *;
  using reference = picture_view;
  using iterator_category = std::forward_iterator_tag;

  using type_it_v = list<bande_it>::iterator;
  using type_it_h = picture::bande_it;
  

  constexpr picture_scanner() noexcept :
      dim_v_(0),
      it_v_(nullptr),
      end_v_(nullptr),
      dim_h_(0)
      it_h_(nullptr),
      end_h_(nullptr)
  {}
  constexpr picture_scanner( 
      const picture & root,
      const picture & pattern ) noexcept : 
      dim_v_(root.dim_v() - pattern.dim_v()),
      it_v_(root._l.begin()),
      end_v_(it_v_ + dim_v_),
      dim_h_(root.dim_h() - pattern.dim_h()),
      it_h_(root._l.front().begin()),
      end_h_(it_h_ + dim_h_)
  {}

  constexpr picture_scanner operator++() noexcept
  {
    if ( it_h_ == end_h_ ) {
      it_h_ = (++it_v_)->begin();
      end_h_ = it_h_ + dim_h;
    } else
      ++it_h_;
    return *this;
  }

  constexpr reference operator*() noexcept 
  {
    return {  it_v_, next(it_v_, dim_v_), 
              it_h_, next(it_h_, dim_h_) };
  }


private:
  std::ptrdiff_t dim_v_, dim_h_;
  type_it_v it_v_, end_v_;
  type_it_h it_h_, end_h_;

};





