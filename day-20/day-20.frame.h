#include <list>
#include <string>
#include <iterator>
#include "day-20.picture.h"


using namespace std;


const list<char> ori_list = {'N', 'E', 'S', 'W'};


struct frame 
{
  typedef list<bool> bande_t;
  typedef list<bande_t>::iterator bande_it;
  typedef list<bande_t>::const_iterator bande_cit;

  frame() {};
  frame( const frame && o, int d = 0, bool r = false ): 
    label(o.label), bandes( o.bandes.size()),
    ori_incr(o.ori_incr+d), picture_(move(o.picture_))
  {
    generate( bandes.begin(), bandes.end(), 
        [&o,i=next( o.bandes.begin(), d )]() mutable {
          if ( i == o.bandes.end() ) 
            i = o.bandes.begin();
          return move(*i++); 
        });
    if ( r )
      for_each( bandes.begin(), bandes.end(),  
          []( bande_t &b ){ b.reverse(); });
  }

  template< char orientation = 'E' >
  frame operator> ( const frame & o ) ; 
  frame operator< ( const frame & o ) ; 
  frame operator^ ( const frame & o ) ; 
  bool operator== ( const frame & o ) const { return o.label == label; }
  bool operator!= ( const frame & o ) const { return o.label != label; }
  operator bool() const { return label != 0; }
  auto begin() const { return bandes.begin(); }
  auto end() const { return bandes.end(); }
  size_t size() const {}

  void operator<< ( int u )  
  {
    for ( ; u-- > 0 ; )
    {
      bandes.push_back( bandes.front() );
      bandes.pop_front();
      ++ori_incr;
    }
  };


  int label = 0;
  list<bande_t> bandes;
  int ori_incr = 0;

  picture picture_;
//  bool reverse;
};


constexpr auto ctob = [](const char c) { return c == '#'; };
constexpr auto btoc = [](const bool b) { return b ? '#' : '.'; };

bool operator>>( istream & in, frame &f ) {
    string l;
    getline( in, l );
    if ( !l.size() )
      return false;
    f.label = stoi( l.substr(5, 4) );

    frame::bande_t n, e, w, s;
    list<frame::bande_t> pic;
    getline( in, l);
    transform( l.begin(), l.end(), back_inserter(n), ctob );

    for ( ; e.push_back( ctob(l[9]) ),
            w.push_front( ctob(l[0]) ),
            pic.emplace_back(),
            transform( ++l.begin(), --l.end(), back_inserter(pic.back()), ctob ),
            in.peek() != '\n'; ) getline( in, l );

    transform( l.rbegin(), l.rend(), back_inserter(s), ctob );
    f.bandes = { n, e, s, w };
    f.picture_ = move(pic);
    in.ignore();
    return true;
}


ostream & operator<<( ostream & out, const frame &f ) {
  out << "label: " << f.label << endl;
  for_each( f.bandes.cbegin(), f.bandes.cend(), 
      [&,o = ori_list.begin()]( const frame::bande_t & b ) mutable {
        out << *o++  << ": ";
        transform ( b.begin(), b.end(), ostream_iterator<char>(out," "), 
            btoc );
        out << endl; 
      });
  return out;
}


/*

namespace std {
  template
  bool list<bool>::operator==( const list<bool> & ) {
    return 
  }
}

*/



template< char orientation >
frame frame::operator> ( const frame & o ) {
  bande_it i = bandes.begin(); 
  bande_cit n;
  find_if( ori_list.begin(), ori_list.end(), 
      [&i] ( const char c ) {
        ++i;
        return c == orientation;
      });
  --i;

  long int off_ori = 2 + 4 - distance( bandes.begin(), i );

  n = find( o.bandes.begin(), o.bandes.end(), *i );
  if ( n != o.bandes.end() ) 
    return { move(o), ( distance( o.bandes.begin(), n) + off_ori ) % 4 };

  i->reverse();
  n = find( o.bandes.begin(), o.bandes.end(), *i );
  i->reverse();
  if ( n != o.bandes.end() ) 
    return { move(o), ( distance( o.bandes.begin(), n) + off_ori ) % 4, true };
  else
    return {};
}

frame frame::operator< ( const frame & o ) {
  return this->operator><'W'>(o);
} 

frame frame::operator^ ( const frame & o ) {
  return this->operator><'N'>(o);
} 



