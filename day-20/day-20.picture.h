#ifndef DAY_20_PICTURE
#define DAY_20_PICTURE
#include <cassert>
#include <list>
#include <iterator>

/**
 *
 *
 *
 */

using namespace std;


struct picture_view;
struct picture_scanner;

struct picture {

  using bande_t   = list<bool>;
  using bande_it  = bande_t::iterator;
  using bande_cit = bande_t::const_iterator;
  using size_type = bande_t::size_type;

  picture() : 
    _l{{}}
  {}
  picture( list<bande_t> && l ) : 
    _l( move(l) )
  {}

  size_type dim_v() const { return _l.size(); }
  size_type dim_h() const { return dim_v() ? _l.front().size() : 0; }
  
  picture & flip_v() noexcept
  { 
    _l.reverse(); return *this; 
  } 
  picture & flip_h() noexcept
  {
    for_each( _l.begin(), _l.end(), 
        []( bande_t & b ) { b.reverse(); });
    return *this;
  }

  picture & rotate_pos() noexcept
  {
    if ( !_l.size() ) return *this;

    list<bande_cit> _l_it( dim_v() );
    transform( _l.begin(), _l.end(), _l_it.begin(), 
        [](const bande_t & b) { return b.rbegin(); });

    list<bande_t> _c( dim_h() );
    generate(_c.begin(), _c.end(), 
        [&_l_it]() mutable { 
          bande_t _nc;
          transform( _l_it.begin(), _l_it.end(), back_inserter(_nc), 
              []( bande_cit i ){ return *i++; });
          return _nc; 
        });

    _l = move(_c);
    return *this;
  }
  picture & rotate_neg() noexcept 
  {
    flip_v();
    rotate_pos();
    flip_v();
    return *this;
  }

  // TODO: optimize this!!
  picture & push_east( picture && p ) 
  {
    assert( p.dim_v() == dim_v() );
    for_each( p._l.begin(), p._l.end(), 
        [i = _l.begin()]( const bande_t &b ) mutable {
          copy( b.begin(), b.end(), back_inserter(*i++) );
        });
    return *this;
  }
  picture & push_west( picture && p ) {
    flip_h();
    p.flip_h();
    push_east( move(p) );
    flip_h();
    return *this;
  }
  friend picture & operator<<( picture & lhs, picture && rhs ) 
  { 
    return lhs.push_east( move(rhs) );
  }
  friend picture & operator>>( picture && lhs, picture & rhs ) 
  { 
    return rhs.push_west( move(lhs) );
  }

  // TODO: optimize me
  picture & push_north( picture && p ) 
  {
    assert( p.dim_h() == dim_h() );
    copy( p._l.rbegin(), p._l.rend(), front_inserter(_l) );
    return *this;
  }
  picture & push_south( picture && p ) 
  {
    flip_v();
    p.flip_v();
    push_north( move(p) );
    flip_v();
    return *this;
  }

private:
  list<bande_t> _l;

  friend picture_view;
  friend picture_scanner;
};



/*
  friend picture_iterator;

  bande_cit find( const picture && p ) {

    assert( dim_h() >= p.dim_h() && dim_v() >= p.dim_v() );
    bande_it s;

    _iterator<dim_v() - p.dim_v(), dim_h() - p.dim_h()> 
      _it( _l.begin(), _l.begin().begin() );

    find_if( _it, _it.end(), 
          [&,i=_l.begin()]( const bande_t & b ){ 
          return any_of( i->begin(), previous(i++->end(), p.dim_h()), 
              [&,j=-1]( const auto v ) mutable { 
              ++j;
              return all_of( p._l.begin(), p._l.end(), 
                    [&,k=i]( const bande_t & _b ){
                    return all_of(_b.begin(), _b.end(), 
                      [&,l=next(k++->begin(), j)]( const bool v ){ 
                        return *(l++) || !v; 
                      });
                  });
            });
        });
  } 

  */



struct picture_view {
  using bande_t = picture::bande_t;
  using type_it_v = list<bande_t>::iterator;
  using type_it_h = picture::bande_it;
  using size_type = picture::size_type;


  picture_view( 
      const type_it_v bv, const type_it_v ev, 
      const type_it_h bh, const type_it_h eh ):
    beg_v_(bv), end_v_(ev),
    beg_h_(bh), end_h_(eh),
    off_h(distance( beg_v_->begin(), beg_h_ )),
    dim_h(distance( beg_h_, end_h_ ))
  {}
  picture_view( picture & pic ):
    beg_v_( pic._l.begin() ), 
    end_v_( pic._l.end() ), 
    beg_h_( pic._l.front().begin() ), 
    end_h_( pic._l.front().end() ),
    off_h(0),
    dim_h( pic.dim_h() )
  {}


  bool operator>=( picture_view & o ) const 
  {
    return mismatch( beg_v_, end_v_, o.beg_v_, 
        [&]( const bande_t & b, const bande_t & ob ){
      return mismatch(  next(b.begin(), off_h), 
                        next(b.begin(), off_h + dim_h),
                        next(ob.begin(), off_h ),
          []( const bool b1, const bool b2 ){ return !b1 || (b1 & b2); }
          ).first == next(b.begin(), off_h + dim_h);
    }).first == end_v_;
  }


private:
  type_it_v beg_v_, end_v_;
  type_it_h beg_h_, end_h_;
  size_type off_h, dim_h;
};


struct picture_scanner {

  using value_type = picture_view;
  using difference_type = std::ptrdiff_t;
  using pointer = picture_view const *;
  using reference = picture_view;
  using iterator_category = std::forward_iterator_tag;

  using type_it_v = list<picture::bande_t>::iterator;
  using type_it_h = picture::bande_it;
  

  picture_scanner() noexcept :
      dim_v_(0),
      it_v_(nullptr),
      end_v_(nullptr),
      dim_h_(0),
      it_h_(nullptr),
      end_h_(nullptr)
  {}
  picture_scanner( 
      picture & root,
      picture & pattern ) noexcept : 
      dim_v_(root.dim_v() - pattern.dim_v()),
      it_v_(root._l.begin()),
      end_v_(next(it_v_, dim_v_)),
      dim_h_(root.dim_h() - pattern.dim_h()),
      it_h_(root._l.front().begin()),
      end_h_(next(it_h_, dim_h_))
  {}

  picture_scanner operator++() noexcept
  {
    if ( it_h_ == end_h_ ) {
      it_h_ = (++it_v_)->begin();
      end_h_ = next( it_h_ , dim_h_ );
    } else
      ++it_h_;
    return *this;
  }

  reference operator*() noexcept 
  {
    return {  it_v_, next(it_v_, dim_v_), 
              it_h_, next(it_h_, dim_h_) };
  }


private:
  std::ptrdiff_t dim_v_, dim_h_;
  type_it_v it_v_, end_v_;
  type_it_h it_h_, end_h_;

};



#endif
