#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <list>
#include <unorderedmap>

#include "day-20.frame.h"


using namespace std;


static const string input = "day-20-input";
//static const string input = "day-20-eg1";
//static const string input = "day-20-eg2";


constexpr int SQUARE_SIZE = 12;
//constexpr int BORDER_SIZE = 10;
//
//
constexpr picture getMonster() {
  picture pic;
  pic << "                  # ";
  pic << "#    ##    ##    ###";
  pic << " #  #  #  #  #  #   ";
}
constexpr picture MONSTER = getMonster();

typedef unordered_map<int,frame> frame_buffer;
typedef list<char>::const_iterator orn_ind;
typedef frame::bande_t      border;
typedef pair<frame &, char>     border_id;
typedef unordered_map<border, list<border_id, border_id>> border_map;



/*
 *
void printCol( ostream &out, const frame_buffer & s ) 
{
  out << "row size: " << s.size() << endl;
  copy( s.begin(), --s.end(), ostream_iterator<frame>(cerr, "\n\n----\n\n") );
  if ( s.size() )
    out <<  s.back() << endl;
}


void printRow( ostream &out, const frame_buffer & s ) 
{
  if ( !s.size() )
    return;

  list<tuple<frame::bande_cit, const frame::bande_cit>> row_it;

  transform( s.begin(), s.end(), back_inserter(row_it), 
      []( const frame & f ){ return make_tuple( f.begin(), f.end() ); });
  
  find_if( row_it.begin(), row_it.end(), 
      [&]( const auto & b ){ 
        return b.first == b.second() || 
          ( copy( *b.first.begin(), *b.first.end(), ostream_iterator<bool>(out) ), 
            out << " ", 0 );
      });

}



template<char o, typename it_dest >
const auto swap_next( frame_buffer& src, it_dest dest ) {
  return [&]( const frame &f ) mutable {

      auto u = find_if( src.begin(), src.end(), 

          [&f,&dest] ( const frame & g ) mutable { 
                frame v = f.operator><o>( g );
                if ( v ) dest = v; 
                return v; 
          });

      if ( u != src.end() ) src.erase(u);
  };
}
*/


namespace std 
{
  struct hash<border> 
  { 
    size_t operator() ( const border & b ) const 
    {
      size_t lhs = accumulate( b.begin(), b.end(), 0, 
          [](const size_t s, const bool b) { return ( s << 1 ) + b; });
      size_t rhs = accumulate( b.rbegin(), b.rend(), 0, 
          [](const size_t s, const bool b) { return ( s << 1 ) + b; });
      return lhs + rhs;
    }
  };
}


struct emplace_border_it 
{
   emplace_border_it( const border_map & bm, const frame &  f):
     brd_map_(bm), frame_(f), oi_( ori_list.begin() ) {}

   void operator=( const border & b ) {
     border_id id = make_pair(frame_, *oi_++); 
     if ( brd_map_.has_key(b) )
       brd_map_[b].push_back( id );
     else 
       brd_map_.emplace( b, { id } );
   }

private:
    border_map & brd_map_;
    frame_ &;
    orn_ind oi_;
};





inline size_t const ori_index( const char c ) {
  return distance( ori_list.begin(), find( ori_list.begin(), ori_list.end(), c));
} 


int main() {

  frame i;
  fstream in(input);
  frame_buffer  frm_buf;
  border_map    brd_map;  
  
  while ( in >> i ) 
  {
    frm_buf.push_back(i);
    copy( i.begin(), i.end(), emplace_border_it(brd_map, i) );
  }

  assert( all_of(brd_map.begin(), brd_map.end(), 
        [](const list<border_id> &l ){ return l.size() <= 2; } ) );


  const auto is_corner = [&brd_map](const frame &f)
  { 
    return count_if( f.begin(), f.end(), 
        [&brd_map]( const border &b )
        {
          return brd_map[b].size() == 1;  // the border is unique
        }) == 2;    // the frame is a corner
  };


  const auto getnext = [&brd_map](const frame &t, const border &b) 
  {
    const frame &f = brd_map[b].front().first;
    if ( f == t )
      f = brd_map[cur_brd].back().first; 
    return const_cast<frame &>( f );
  }

  assert( count_if(frm_buf.begin(), frm_buf.end(), is_corner) == 4 );    

  frame & topleft = *find_if( frm_buf.begin(), frm_buf.end(), is_corner );

  border & cur_brd = *find_if( topleft.begin(), topleft.end(), 
      [&brd_map](const border &b) { return brd_map[b].size() == 1; } );
  
  topleft << ( 3 - ori_index(cur_brd.second) );

  picture full_pic;
  int j = 0;
  while ( ++j,
      brd_map[ topleft.south() ].size() > 1 )
  {
      picture row_pic;
      frame & top = topleft, const & f; 
      int i = 0;
      while ( ++i,
          picture row_pic.push_east( top.picture() ),
          cur_brd = *++top.begin(),
          brd_map[cur_brd].size() > 1 )
      {
          top = getnext( top, cur_bdr );
          top << ( 3 - distance(f.begin(), find( f.begin(), f.end(), cur_brd )));
      }

      assert( i == SQUARE_SIZE );
      full_pic.push_south( row_pic );
      topleft = getnext( topleft, topleft.south() );
  }

  assert( j == SQUARE_SIZE );

  cout << full_pic;

  picture_view v( full_pic );


  v.search( )


//  cout << accumulate(c.begin(), c.end(), 1, multiplies<T>() ) << endl;
}
