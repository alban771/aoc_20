#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cstring>


using namespace std;


static const string input = "day-18-input";
//static const string input = "day-18-eg1";
//static const string input = "day-18-eg2";


typedef unsigned long long T;
/*
istream & operator>>( istream& in,  T ){
}

ostream & operator>>( ostream& in,  T ){
}
*/


typedef vector<T> container;


struct eval
{
  T a, b = 1;
  char op = '.';

  const T operator()(const string &line) 
  {
  
    for ( int i = 0; i < line.size(); ++i, ++i )
    {
      char c = line[i];
      if ( is_num(c) )
        apply( (int)c - 0x30 );

      else if ( c == '+' || c == '*' )
        op = c;

      else if ( c == '(' )
      {
        int u = 0, v = i;
        do
          v = line.find(')', v+1);
        while ( count( next( line.begin(), i ), next (line.begin(), v), '(') > ++u ); 

        string sub = line.substr(i+1, v - i - 1);
        apply( eval()(sub) );
        i+= sub.size() - 1;
      }
    }
    return a * b;
  }

  bool is_num( char c ) {
    return (int)c > 0x2F && (int)c < 0x3A ;
  }

  void apply( const T i ) {
    if ( op == '.' )
      a = i;
    else if ( op == '+' )
      a += i;
    else if ( op == '*' )
    {
      b *= a;
      a = i;
    }

  }

};


int main() {

  string line;
  fstream in(input);
  container c;
  
  while ( getline( in, line ) ) 
  {
    c.push_back( eval()(line) );
  }


  cerr << "[ " ;
  copy(c.begin(), --c.end(), ostream_iterator<T>(cerr, ", ") );
  cerr <<  c.back() << " ]" << endl;


  cout << accumulate(c.begin(), c.end(), 0 ) << endl;
}
