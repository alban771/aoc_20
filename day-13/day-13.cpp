#include <iostream>
#include <fstream>

#include <vector>
#include <set>
#include <map>
#include <string>
#include <sstream>

#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>
#include <gmp.h>



using namespace std;


static const string input = "day-13-input";
//static const string input = "day-13-eg1";

constexpr int size_bigint = 20;



typedef unsigned long int T;
typedef mpz_t bint;
/*
istream & operator>>( istream& in,  T ){
}

*/


typedef vector<pair<T,T>> container;

ostream & operator<<( ostream& out, pair< T, T> & e ){
  return out <<  "( " << e.first << "," <<  e.second << " )";
}




int main() {

  T   i, h;
  fstream in(input);
  container  c, d;
  
  in >> h;
  in.ignore();
  string l;
  getline( in, l );
  transform( l.begin(), l.end(), l.begin(), 
      [](const char v){ return ( v == 'x' ) ? '0' : (v == ',') ? ' ': v;});
  stringstream ss(l);
  
  T p;
  p = 0;
  while ( ss >> i ) 
  {
    if (i > 0) c.push_back( make_pair( p % i, i));
    ++p;
  }


  cerr << "c : [ "  << endl;
  for_each( c.begin(), c.end(), []( auto p ){ cerr << p << endl; });
  cerr <<  " ]" << endl;

  //auto M = max_element(c.begin(), c.end() 
      //,[&h]( const T j, const T k ){ return j - (h % j) < k - (h % k); } 
  //    );
  //cerr << *M << endl;

  bint _u, _v;
  mpz_inits(_u, _v, NULL);
  mpz_set_ui( _u, c.begin()->second );
  mpz_set_ui( _v, c.begin()->second );

  for_each( ++c.begin(), c.end(),  
      [&_u,&_v]( const container::value_type &e) {

        char s1[size_bigint], s2[size_bigint];
        mpz_get_str( s1, 10, _u);
        mpz_get_str( s2, 10, _v);
        cerr << "e: (" << e.first << ", " << e.second << ")";
        cerr << "\t\ts: ( " <<  s1 << ", " << s2 << " )" << endl;

        T k = 0;
        bint _b, _e;
        mpz_inits(_b, _e, NULL);
        mpz_set( _b, _u);

        while ( mpz_cdiv_r_ui(_b, _b, e.second), mpz_get_ui(_b) != e.first ) 
        { 
          ++k; 
          mpz_add( _b, _b, _v); 
        }


        mpz_addmul_ui( _u, _v, k );
        mpz_mul_ui( _v, _v, e.second ); // hyp : numbers first between them. Otherwise is lcm.

      });
  

  char s[size_bigint];
  mpz_get_str(s, 10, _u);
  cout << s << endl;

}
