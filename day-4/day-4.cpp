#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <map>
#include <algorithm>
#include <numeric>
#include <iterator>


using namespace std;

typedef set<string> fields;
typedef map<string,string> passport;
typedef passport::value_type entry;

static const fields reqt {
   "byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid" 
};

static const string _hex = "0123456789abcdef";
static const fields ecl { 
  "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
};

const auto is_valid = []( const entry& e ) {
  const string s = e.second;
  cerr << e.first << ", ";
  if ( e.first == "byr" ) {
    int y = stoi( s );
    return y >= 1920 && y <= 2002; 

  } else if ( e.first == "iyr" ) {
    int y = stoi( s );
    return y >= 2010 && y <= 2020; 

  } else if ( e.first == "eyr" ) {
    int y = stoi( s );
    return y >= 2020 && y <= 2030; 

  } else if ( e.first == "hgt" ) {
    int h = stoi( s.substr(0, s.size()-2));
    string u = s.substr(s.size()-2, 2);
    cerr << "height:" << h << " unit:" << u ;
    if ( u == "cm" ) 
        return h >= 150 && h <= 193; 
    if ( u == "in" ) 
        return h >= 59 && h <= 76; 
    return false;

  } else if ( e.first == "hcl" ) {
    //size_t pos = 1;
    auto b = s.begin();
    ++b;
    try {
      //return s[0] == '#' && stoi( s, &pos, 16 ) > 0;
      return s[0] == '#' && all_of( b, s.end(), 
          []( const char c){ return _hex.find(c) != string::npos; });
    } catch (...) {
      return false;
    }
  } else if ( e.first == "ecl" ) {
    cerr << " ecl_d:" << s ;
    return ecl.find( s ) != ecl.end();

  } else if ( e.first == "pid" ) { 
    try {
      cerr << " pid: " << s.size() << (stoll(s) > 0ll);
      return s.size() == 9 && stoll(s) > 0ll;
    } catch (...) {
      return false;
    }
  }

  return true;
};



template<class map_type>
class tag_iterator : public iterator<forward_tag_iterator,
                                      map_type::key_type >{
  map_type::iterator inner;
  public: 
    tag_iterator( map_type::iterator i ) inner(i) {};
    tag_iterator& operator++() { ++inner; return *this; }
    reference operator*() { return get<0>(inner); }
    bool operator==( tag_iterator o ) const { return inner == o.inner; }
    bool operator!=( tag_iterator o ) const { return !(*this == o); }
} 


template<class map_type>
tags( const map_type& ) -> pair< tag_iterator<map_type>, tag_iterator<map_type> > 
=
[]( const map_type& m ){ return { tag_iterator(m.begin()), tag_iterator(m.end()) }; };




typedef map<string,string> passport;

static const fields reqt {
   "byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid" 
};




struct passport {

  typedef map<string,string> tagvalue;

  typedef static const pair<int,int> range;
  typedef static const set<string> opt; 
  
  tagvalue  fields;
  range     byr(1920,2002), iyr(2010,2020), eyr(2020,2030), hgt_cm(150,193), hgt_in(59,76);
  opt       req{"byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"};
  opt       hgt_u{"in", "cm"};
  opt       ecl{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
  
  void operator<<( const string& s ){
    stringstream ss(s);
    string tv, t, v;
    int j;
    while ( ss >> tv ){
      j = tv.find(':');
      fields.emplace(tv.substr(0, j), tv.substr(j+1,tv.size()-j));
    }
  }

  pair< tag_iterator, tag_iterator > tags() const {
    set<string> s;
    transform( set )
  }

  bool 
}


int main () {

  string s, f;
  passport p;
  fields keys;
  int cpt = 0;
  fstream in("day-4-input");
  
  while ( getline(in, s) ) { 
    if ( !s.size() ) {
//      cerr << "passport: { ";
//      copy( p.begin(), p.end(), ostream_iterator<string>(cerr, " ") );
//      cerr << "}";
      keys.clear();
      transform( p.begin(), p.end(), inserter( keys, keys.begin() ), 
            []( const entry& e) { return e.first;} );
      if ( includes( keys.begin(), keys.end(), reqt.begin(), reqt.end())
          && all_of( p.begin(), p.end(), is_valid )
          ) {
        cerr << " ok \n";
        ++cpt;
      } else
        cerr << " x \n";

      p.clear();
    }
    else { 
      int c =0;
      while( c != string::npos && c != s.size()  )
      {
        int beg = c == 0 ? 0 : c+1, sep = s.find(':', c), end = s.find(' ', ++c);
        string u = s.substr(beg, sep-beg), v =  s.substr( sep+1,end-sep - ( s[end] == ' ' ) );
        //v = v.back() == ' ' ? v.substr(0,v.size()-1): v;
        p.emplace( u , v );
        c = end;
        cerr << "field: " << u << "/" << v << endl; 
      }
    }
  }

  cout << cpt << endl;
}
