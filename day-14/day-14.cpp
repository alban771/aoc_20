#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <bitset>
#include <map>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>
#include <gmp.h>




using namespace std;


static const string input = "day-14-input";
//static const string input = "day-14-eg2";


typedef bitset<36> T; 

static T mask;


struct less_bs {
  bool operator()(const T& t1,const T& t2) const {
    return t1.to_string() < t2.to_string();
  }
};

typedef map< T, size_t, less_bs  > container;


struct line {

  string cmd, val; 
};




istream & operator>>( istream& in, line & l ){
  string s;
  if ( !getline (in , s) ) return in;
    
  l.cmd = s.substr( 0, s.find('=')-1 );
  l.val = s.substr( s.find('=')+2, s.size() - s.find('=')-2);
  return in;
}



int main() {

  fstream in(input);

  line i; 
  T mask_x, mask_1;
  container  c;
  
  while ( in >>  i ) 
  {
    if ( i.cmd == "mask" )
    {
      for ( int j =0; j < 36; ++j){ 
        mask_x[j] = i.val[35 - j] == 'X';
        mask_1[j] = i.val[35 - j] == '1';
      }
      cerr << "mask_x: " << mask_x << endl;
      cerr << "mask_1: " << mask_1 << endl;
    }
    else 
    {
      size_t pos = stoul( i.cmd.substr( 4, i.cmd.find(']') - 4 ));
      size_t val = stoul( i.val );

      T bs( pos );
      bs |= mask_1;

      vector<T> cc{ bs };

      for ( int j = 0; j < 36; ++j ){
        if ( mask_x[j] ) {

          size_t ccs = cc.size();
          cc.resize( 2*ccs );
          auto e = next(cc.begin(), ccs);

          transform( cc.begin(), e, e, 
              [j]( T bs ){ bs[j] = !bs[j]; return  bs; });
        }
      }

      for_each( cc.begin(), cc.end(), 
          [&c, &val]( const T &p ){ 
            cerr << "c[ " << p << " ] = " << val << endl;
            c[p] = val; 
          });
      
    }
  }


  //cout << accumulate(c.begin(), c.end(), 0, []( const unsigned long long s, const auto p ){ return s + p.second; } ) << endl;

  mpz_t s;
  mpz_init(s);
  mpz_set_ui(s, 0);

  for_each( c.begin(), c.end(), [&s]( const auto & p ) mutable {
        mpz_add_ui( s, s, p.second);
      }); 

  char show[20];
  mpz_get_str(show, 10, s);

  cout << show << endl;


}
