#include <list>
#include <vector>
#include <set>
#include <tuple> // tie
#include <iterator>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iostream>

using namespace std;



void show_p( list<int>& p, int k ){
    cerr << "Player " << k << "'s deck: "; 
    copy( p.rbegin(), --p.rend(), ostream_iterator<int>(cerr, ", " ));
    cerr << p.front() << endl;
}



namespace std {

  template<>
  struct less<vector<int>> 
  {
    bool operator()( const vector<int>& v1, const vector<int>& v2 ) const {

      auto m1 = v1.begin(), m2 = v2.begin();
      if ( v1.size() < v2.size() )
        tie(m1, m2) = mismatch(v1.begin(), v1.end(), v2.begin() );
      else
        tie(m2, m1) = mismatch(v2.begin(), v2.end(), v1.begin() );

      return m2 != v2.end() && ( m1 == v1.end() || *m1 < *m2 );
    }
  };

  template<>
  struct less<pair<vector<int>, vector<int>>> 
  {
    using pair_t = pair<vector<int>, vector<int>>;
    bool operator()( const pair_t& p1, const pair_t& p2 ) const {
      return p1.first < p2.first || ( ! (p2.first < p1.first) && p1.second < p2.second );
    }
  };

}


vector<int> cpy_to_vec( const list<int>& l ) 
{
  vector<int> ret( l.size() );
  copy( l.begin(), l.end(), ret.begin() );
  return ret;
}

pair<vector<int>, vector<int>> make_record( const list<int>& p1, const list<int>& p2)
{
  return make_pair( cpy_to_vec(p1), cpy_to_vec(p2) );
}


pair<list<int>, list<int>> combat( const list<int>& _p1, const list<int>& _p2, int _x1, int _x2, const int id = 1) 
{
  list<int> p1(_x1), p2(_x2);
  copy_n(_p1.rbegin(), _x1, p1.rbegin());
  copy_n(_p2.rbegin(), _x2, p2.rbegin());
  set<pair<vector<int>,vector<int>>> records;
  int sub_id = id;
  cerr << endl;
  cerr << "=== Game " << id << " ===" << endl;
  while( p1.size() &&  p2.size() )
  {
    cerr << endl;
    cerr << "-- Round " << records.size() + 1 << " (Game " << id << ") --" << endl;
    show_p( p1, 1 );
    show_p( p2, 2 );
    cerr << endl;
    
    auto rec = make_record(p1, p2);
    if ( records.find(rec) != records.end() )
      return make_pair( move(p1), list<int>{} );

    records.insert( move(rec) );

    int x1 = p1.back(), x2 = p2.back();
    cerr << "Player 1 plays: " << x1 << endl;
    cerr << "Player 2 plays: " << x2 << endl;
    p1.pop_back();
    p2.pop_back();
    bool p1_win;
    if ( x1 <= p1.size() && x2 <= p2.size() )
    {
      cerr << "Playing a sub-game to determine the winner..." << endl;
      p1_win = !combat( p1, p2, x1, x2, ++sub_id ).first.empty();
    }
    else
    {
      p1_win = x1 > x2;
    }

    if ( p1_win )
    {
      cerr << "Player 1 wins round " << records.size() +1 << " of game " << id << "!" << endl;
      p1.push_front( x1 );
      p1.push_front( x2 );
    }
    else
    {
      cerr << "Player 2 wins round " << records.size() +1 << " of game " << id << "!" << endl;
      p2.push_front( x2 );
      p2.push_front( x1 );
    }
  }
  return make_pair(move(p1),move(p2));
}


int main() {

  fstream in("input");

  list<int> p1, p2;

  {
    int k;
    string n;

    getline(in, n); //Player 1:
    while( in >> k ) p1.push_front(k);
    in.clear();
    getline(in, n); //Player 2:
    while( in >> k ) p2.push_front(k);
  }


  cerr << "p1 #" << p1.size() << endl;
  cerr << "p2 #" << p2.size() << endl;

  tie(p1,p2) = combat(p1, p2, p1.size(), p2.size());

  p1.splice( p1.begin(), p2 );

  show_p( p1, 0 );

  cout << accumulate( p1.begin(), p1.end(), 0, 
      [i=0]( int s, int h ) mutable 
      { 
        return s + (++i)*h; 
      }) << endl;

}
