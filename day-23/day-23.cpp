#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <string>
#include <complex>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>


using namespace std;
using namespace std::complex_literals;


//static const string input = "day-23-input";
static const string input = "day-23-eg1";


typedef complex<double> T; 

struct tile {
  T t;
};

static const double PI = acos(-1);
static const T thetha = exp( 1i * ( PI / 3.0 ) );

namespace std {
  template<>
  struct hash<T> {
    size_t operator()( const T & c ) const {
      return ((size_t) roundl( c.real() / 0.5 ) << 8) + 
              (size_t) roundl( c.imag()/ thetha.imag() );   
    }
  };
}





istream & operator>>( istream& in, tile &t ){
  char c;
  T i = 1.;
  t.t = 0.;
  bool finish = false;
  while ( !finish && in >> c ) {
    switch ( c ) {
      case 'n':
        i = thetha;
        break;
      case 's':
        i = conj(thetha);
        break;
      case 'w':
        i.real( -i.real() );
      case 'e':
        t.t += i;
      default:
        i = 1.;
        finish=true;
    }
  }
  return in;
}



int main() {

  fstream   in(input);
  tile      t;
  unordered_map<T, int> occ;


  while ( in >> t ) 
  {
    occ.try_emplace(t.t, 0);
    ++occ[t.t];
  }
  
  cout << count_if(occ.begin(), occ.end(), 
      []( const auto &p){ return p.second % 2; }) << endl;
}
