#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <execution>

using namespace std;


static const string input = "day-16-input";
//static const string input = "day-16-eg1";


constexpr int FIELD_NUMBER = 20;


typedef unsigned long long   T;
typedef pair<T,T>   ivl;
constexpr auto field_range = [](){ 
  vector<int> r(FIELD_NUMBER);
  generate( execution::seq, r.begin(), r.end(), [i=0]() mutable { return i++; });
  return r;
};

struct rule {
  string l;
  bool solve = false;
  ivl i1, i2;
  vector<int> candidate = field_range();
  
  void clear() { l = ""; }
  bool contains( T v ) const {
    return ( i1.first <= v && i1.second >= v ) || ( i2.first <= v && i2.second >= v );
  }
};

istream & operator>>( istream& in,  rule & r ){
  r.clear();
  getline( in, r.l );
  if ( !r.l.size() ) return in;
  int x = r.l.find(":");
  stringstream ss( r.l.substr(x + 1, r.l.size() - x - 1) );
  string _or;
  ss >> r.i1.first;
  ss.ignore();
  ss >> r.i1.second >> _or >> r.i2.first;
  ss.ignore();
  ss >> r.i2.second;
  r.l = r.l.substr(0, x);
  return in;
}




ostream & operator<<( ostream& out, const ivl & i ){
  return out << i.first << " - " << i.second;
}


typedef map<string, rule> container;
typedef vector<T>   ticket;


istream & operator>>( istream & in, ticket & t ){
  t.clear();
  string s;
  getline( in, s );
  if ( !s.size() ) return in;

  stringstream ss (s);
  T k;
  while ( ss >> k ) {
    t.push_back(k);
    ss.ignore();
  }
  return in;
}



int main() {

  rule i;
  fstream in(input);
  container  c;
  
  while ( in >> i ) 
  {
    if ( !i.l.size() )
      break;
    c.emplace(i.l, i);
  }


  cerr << "rules: [ " << endl;
  for_each( c.begin(), c.end(), []( const auto p ) {
      cerr << p.first << ": " << p.second.i1 << " || " << p.second.i2 << endl;
      });
  cerr << " ] end rules" << endl;


  string s;
  ticket my_ticket, t;
  vector<ticket> vt;

  getline( in, s  ); // your ticket: 
  in >> my_ticket;
  getline( in, s  );
  getline( in, s  ); // nearby tickets:

  while ( in >> t ) {
    vt.push_back(t);
  }


  cerr << endl;
  cerr << "tickets: [" << endl;
  for_each( vt.begin(), vt.end(), 
      [](const ticket &t) { copy(t.begin(), t.end(), ostream_iterator<T>(cerr,",")); cerr << "\n";});
  cerr << " ] end tickets" << endl;


  /*
  cout << accumulate(vt.begin(), vt.end(), 0, 
      [&]( const T s, const ticket & t ) { cerr << "\n"; return s + accumulate( t.begin(), t.end(), 0,
        [&]( const T s, const T v ) { 
          auto r = find_if( c.begin(), c.end(), 
            [v]( const auto & p ){ return p.second.contains(v); });
          if ( r == c.end() ) 
          {
            cerr << v << ",";  
            return s + v; 
          } 
          else 
            return s;
        });
      }) << endl;
      */


  auto r = remove_if( vt.begin(), vt.end(), 
      [&]( const ticket & t ) { return any_of( t.begin(), t.end(),
        [&]( const T v ) { return none_of( c.begin(), c.end(), 
            [v]( const auto & p ){ return p.second.contains(v); });
        });
      });
  vt.erase( r, vt.end() );


  for_each( c.begin(), c.end(), 
      [&vt]( auto & p){ 
      auto r = remove_if( p.second.candidate.begin(), p.second.candidate.end(), 
        [&vt,&p]( const int f){ return any_of( vt.begin(), vt.end(), 
          [&p,&f]( const ticket &t ){ return !p.second.contains(t[f]); }
          ); }
        );  
      p.second.candidate.erase( r, p.second.candidate.end() ); }
  );


  bool stable = false;
  while (!stable) {
    stable = true;

    for_each( c.begin(), c.end(), 
        [&c,&stable]( auto & p){ 
          if ( !p.second.candidate.size()  ) 
            throw "No candidate for this rule";
          if ( !p.second.solve && p.second.candidate.size() == 1 )
          {
            stable = false;
            p.second.solve =  true;
            for_each( c.begin(), c.end(), 
                [&p]( auto &q ){ 
                  if ( q.first != p.first )
                  {
                    auto f = find( q.second.candidate.begin(), q.second.candidate.end(), p.second.candidate.front());
                    if ( f != q.second.candidate.end() )
                      q.second.candidate.erase( f ); 
                  }
                });
          }
        });
  }
  

  cerr << "candidates: [" << endl;
  for_each( c.begin(), c.end(), []( const auto p ) {
      cerr << p.first << ": (";
      copy( p.second.candidate.begin(), --p.second.candidate.end(), ostream_iterator<int>(cerr, ","));
      cerr << p.second.candidate.back() <<  ")" << endl;
      });
  cerr << " ] end candidates" << endl;



  cout << accumulate( c.begin(), c.end(), 1, 
      [&my_ticket]( const T s, const auto &p ) { 
      if ( p.first.substr(0,3) == "dep")
        return s * my_ticket[ p.second.candidate.front() ];
      else
        return s; 
      }) << endl; 


}
