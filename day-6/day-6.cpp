#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;


static const string Input = "day-6-input";


typedef int T;


int main() {

  T   i = 0;
  string l;
  fstream in(Input);
  vector<string>   g;

  
  while ( getline( in, l) ) 
  {
    if ( l.size())
      g.push_back(l);
    else {
      for ( char c = 'a'; c<='z'; ++c ) {
        if ( all_of( g.begin(), g.end(), 
            [&c](const string & m){ return m.find(c) != string::npos; }) ) {
          cerr << c;
          ++i;
        }
      }
      cerr << endl;
      g.clear();
    }
  }

  cout << i << endl;

  //cerr << "[ " << copy(c.begin(), --c.end(), ostream_iterator<T>(cerr, ", ") );
  //cerr <<  c.back() << " ]" << endl;


  //cout << accumulate(c.begin(), c.end(), 1, multiplies<T> ) << endl;
}
